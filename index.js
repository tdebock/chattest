var mongoose = require('mongoose')
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var Message = require('./test-api/models/Message.js');
var MongoClient = require('mongodb').MongoClient;
var dbm;

mongoose.Promise = global.Promise;
MongoClient.connect('mongodb://localhost/tt3-dev', function(err, db){
	if(err){
		console.log('failed to connect to mongo' + err);
	} else{
    console.log('Connected to mongodb!');
    dbm = db;
	}
});

io.on('connection', function(socket){
  // console.log('a user connected');
  socket.on('disconnect', function(){
		// console.log('a user disconnected');
  });

	socket.on('join_room', function (room) {
	        socket.join(room);
					// console.log('joined room: ' + room);
	});

	socket.on('leave_room', function (room) {
	        socket.leave(room);
					// console.log('left room: ' + room);
	});

  socket.on('chat message', function(msg){
		gotMessage(msg);
  });
});

function gotMessage(msg) {
	var obj = JSON.parse(msg);
	var messageString = obj['message'];
	var userIDString = obj['user_id'];
	var usernameString = obj['username'];
	var fleetIDString = obj['fleet_id'];
	var chat_room_id = obj['chat_room_id'];
	var dateString = Date.now();

	obj['created_at'] = Date.now();

	io.sockets.in('browser_listener').emit('test listener', obj);

	if (chat_room_id != 'global_chat') {
		var collection = dbm.collection('messages');
		collection.insert({
			user_id: userIDString,
			username: usernameString,
			message: messageString,
			fleet_id: fleetIDString,
			chat_room_id: chat_room_id,
			created_at: Date.now() }, function(err,docsInserted){

				var idArray = docsInserted['insertedIds'];
				var id = idArray[0];
				obj['_id'] = id;

				if (chat_room_id == '') {
					io.sockets.in('fleet=' + fleetIDString).emit('new message', obj);
				} else {
					io.sockets.in(chat_room_id).emit('new message', obj);
				}

			});
	} else {
		io.sockets.in(chat_room_id).emit('new message', obj);
	}
}

function listenToRoom() {
	console.log('that worked!');
}

app.get('/', function(req, res){
  res.sendfile('index.html');
});

http.listen(8080, function(){
  console.log('listening on *:8080');
});

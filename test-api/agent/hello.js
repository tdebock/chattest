var agent = require('./_header')
  , device = require('../device');

  // Create a badge message
  agent.createMessage()
    .device(device)
    .alert('bade test!')
    .badge(+1)
    .send();

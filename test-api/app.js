var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var authentication = require('./routes/authentication');
var index = require('./routes/index');
var ota = require('./routes/ota');
var users = require('./routes/users');
var buildings = require('./routes/buildings');
var fleets = require('./routes/fleets');
var messages = require('./routes/messages');
var chatrooms = require('./routes/chatrooms');
var traderequests = require('./routes/traderequests');
var news = require('./routes/news');
var rewards = require('./routes/rewards');
var inboxitems = require('./routes/inboxitem');
var friendslist = require('./routes/friendslist');

var socketListener = require('./socketListener.js');

// var remoteNotificationHeader = require('./agent/_header.js');
var remoteNotification = require('./agent/remoteNotification.js');
// var mockFeedback = require('./feedback/mock.js');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file

var app = express();

// load mongoose package
var mongoose = require('mongoose');

mongoose.connect(config.database, function(err){
	if(err){
		console.log(err);
	} else{
    	console.log('Connected to mongodb!');
	}
});

app.set('superSecret', config.secret);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// app.use(bodyParser.json());
app.use(bodyParser.text({
    verify: function(req, res, buf, encoding) {
				if (buf && buf.length) {
				    req.rawBody = buf.toString(encoding || 'utf8')
				}
    }
}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', ota);

// app.use('/authentication', authentication);
// app.use('/', index);
// app.use('/beta', ota);
// app.use('/users', users);
// app.use('/buildings', buildings);
// app.use('/fleets', fleets);
// app.use('/messages', messages);
// app.use('/chatrooms', chatrooms);
// app.use('/traderequests', traderequests);
// app.use('/news', news);
// app.use('/rewards', rewards);
// app.use('/inboxitem', inboxitems);
// app.use('/friendslist', friendslist);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

exports.tradeRequestSent = function(tradeRequest) {
		socketListener.receiveTradeRequest(tradeRequest);
};
exports.tradeRequestUpdated = function(tradeRequest, modifications) {
	socketListener.updateTradeRequest(tradeRequest, modifications);
};

exports.inboxItemSent = function(inboxItem) {
	socketListener.receiveInboxItem(inboxItem);
};

exports.sendRemoteNotification = function(notification) {
	remoteNotification.sendRemoteNotificationToClient(notification);
};

module.exports = app;

var apnagent = require('apnagent')
  , feedback = new apnagent.MockFeedback();

  // pull in your device
  var device = require('../device');

  // unsub it as of 30 minutes ago
  feedback.unsub(device, '-30m');

feedback
  .set('interval', '30s') // default is 30 minutes?
  .connect();

  // feedback.set('concurrency', 1);

  /*!
 * @param {apnagent.Device} device token
 * @param {Date} timestamp of unsub
 * @param {Function} done callback
 */

feedback.use(function (device, timestamp, done) {
  var token = device.toString()
    , ts = timestamp.getTime();

    console.log(token + ' unsubscribed');

  // psuedo db code
  // db.query('devices', { token: token, active: true }, function (err, devices) {
  //   if (err) return done(); // bail
  //   if (!devices.length) return done(); // no devices match
  //
  //   // async forEach
  //   devices.forEach(function (device, next) {
  //     // this device hasn't pinged our api since it unsubscribed
  //     if (device.get('timestamp') <= ts) {
  //       device.set('active', false);
  //       device.save(next);
  //     }
  //
  //     // we have seen this device recently so we don't need to deactive it
  //     else {
  //       next();
  //     }
  //   }, done);
  // });
});

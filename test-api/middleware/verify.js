var config = require('../config');
var base64 = require('base-64');
var base64Lite = require('base64-lite');
var utf8 = require('utf8');

function setCharAt(str,index,chr) {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}

function scramble(input) {
  for (var i = 0; i < input.length; i++) {
      if (i*3 + 2 > input.length - 1) {
          break;
      };
      var temp = input.substr(i*3 + 2,1)
      input = setCharAt(input,i*3 + 2,input.substr(i*3,1));
      input = setCharAt(input,i*3,temp);
  };
  return input;
}

function xorCipher(input, key) {
  var arr = [];
  for (var i = 0; i < input.length; i++) {
      var charCode = input.charCodeAt(i) ^ key[i % key.length].charCodeAt(0);
      arr.push(String.fromCharCode(charCode));
  }
  return arr.join("");
}

exports.encrypt = function(input) {
  var encoded = base64Lite.btoa(input);

  var key = config.encrypt_key1;
  var key2 = config.encrypt_key2;

  encoded = scramble(encoded);
  var strFirstPass = xorCipher(encoded, key2);
  strFirstPass = scramble(strFirstPass);
  var encrypted = xorCipher(strFirstPass, key);
  return encrypted;
};

exports.decrypt = function(input) {
  var key = config.encrypt_key1;
  var key2 = config.encrypt_key2;

  input = scramble(input);
  var strFirstPass = xorCipher(input, key2);
  strFirstPass = scramble(strFirstPass);
  var decrypted = xorCipher(strFirstPass, key);

  var bytes = base64.decode(decrypted);
  var decoded = utf8.decode(bytes);

  return decoded;
};

exports.validate = function(input, fields) {
  if (!input) {
    return false;
  }
  for(var i = 0; i < fields.length; i++){
    if(!input.hasOwnProperty(fields[i])){
      return false;
    }
  }
  return true;
}

var mongoose = require('mongoose');
var BuildingSchema = new mongoose.Schema({
  user_id: String,
  building_id: Number,
  type: Number,
  level: Number,
  pos_x: Number,
  pos_y: Number,
  local_db_id: Number,
  date_upgrade_started: Date,
  date_last_visited: Date,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Building', BuildingSchema);

var mongoose = require('mongoose');
var ChatRoomSchema = new mongoose.Schema({
  name: String,
  admin_id: String,
  allows_member_invites: Number,
  created_at: Number,
  members: Array,
  pending: Array,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('ChatRoom', ChatRoomSchema);

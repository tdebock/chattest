var mongoose = require('mongoose');
var FleetSchema = new mongoose.Schema({
  name: String,
  level: Number,
  type: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Fleet', FleetSchema);

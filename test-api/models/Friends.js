var mongoose = require('mongoose');
var FriendsSchema = new mongoose.Schema({
  status: Number,
  issuer_username: String,
  issuer_id: String,
  receiver_username: String,
  receiver_id: String,
  created_at: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Friends', FriendsSchema);

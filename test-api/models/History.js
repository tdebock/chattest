var mongoose = require('mongoose');
var HistorySchema = new mongoose.Schema({
  attacker_username: String,
  attacker_id: String,
  defender_username: String,
  defender_id: String,
  outcome: Number,
  created_at: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('History', HistorySchema);

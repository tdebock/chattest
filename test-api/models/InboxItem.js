var mongoose = require('mongoose');
var InboxItemSchema = new mongoose.Schema({
  issuer_id: String,
  issuer_username: String,
  receiver_id: String,
  receiver_username: String,
  message: String,
  type: Number,
  taggedID: String,
  created_at: Number,
  seen: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('InboxItem', InboxItemSchema);

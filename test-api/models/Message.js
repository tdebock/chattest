var mongoose = require('mongoose');
var MessageSchema = new mongoose.Schema({
  user_id: String,
  username: String,
  message: String,
  type: Number,
  fleet_id: String,
  chat_room_id: String,
  created_at: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Message', MessageSchema);

var mongoose = require('mongoose');
var NewsSchema = new mongoose.Schema({
  message: String,
  type: Number,
  created_at: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('News', NewsSchema);

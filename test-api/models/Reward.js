var mongoose = require('mongoose');
var RewardSchema = new mongoose.Schema({
  user_id: String,
  amount: Number,
  type: Number,
  created_at: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Reward', RewardSchema);

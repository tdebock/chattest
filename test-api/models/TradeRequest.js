var mongoose = require('mongoose');
var TradeRequestSchema = new mongoose.Schema({
  issuer_id: String,
  issuer_username: String,
  receiver_id: String,
  receiver_username: String,
  status: Number,
  issuer_accept: Number,
  receiver_accept: Number,
  receiver_loot: Array,
  issuer_loot: Array,
  issuer_removed: Number,
  receiver_removed: Number,
  created_at: Number,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('TradeRequest', TradeRequestSchema);

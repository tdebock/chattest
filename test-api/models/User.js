var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
  username: String,
  fleet_id: String,
  ios_udid: String,
  firebase_token: String,
  updated_at: { type: Date, default: Date.now },
});
module.exports = mongoose.model('User', UserSchema);

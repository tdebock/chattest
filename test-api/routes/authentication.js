var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var config = require('../config');
var Verify = require('../middleware/verify.js');


router.get('/', function(req, res, next) {
        if (req.headers['secret'] === config.secret) {
          var token = jwt.sign({}, config.secret, {
            expiresIn: 60*60*24
          });
          res.send(Verify.encrypt(JSON.stringify({
            token: token,
            current_time: Date.now()
          })));
        } else {
          var err = new Error('Unauthorized');
          err.status = 403;
          return next(err);
        }
});

router.get('/currentTime', function(req, res, next) {
  res.send(Verify.encrypt(JSON.stringify({
    current_time: Date.now()
  })));
});

module.exports = router;

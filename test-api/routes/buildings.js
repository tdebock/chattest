var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Building = require('../models/Building.js');
var Verify = require('../middleware/verify.js');
var ObjectId = require('mongodb').ObjectId;

var jwt = require('jsonwebtoken');
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        req.decoded = decoded;
        if (req.rawBody) {
          var decrypted = Verify.decrypt(req.rawBody);
          try {
            req.decryptedBody = JSON.parse(decrypted);
          } catch (e) {
            console.log(e);
            var err = new Error('Unauthorized');
            err.status = 403;
            return next(err);
          }
        }
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});

router.get('/', function(req, res, next) {
  Building.find(function (err, building) {
    if (err) return next(err);
    res.json(building);
  });
});

router.get('/user_id/:user_id', function(req, res, next) {
  Building.find({user_id: req.params.user_id}, function (err, building) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(building)));
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  var obj = req.decryptedBody;
  var fields = ['user_id', 'buildingData'];
  if (!Verify.validate(obj, fields)) {
    var err = new Error('Unauthorized');
    err.status = 403;
    err.stackTraceLimit = 0;
    return next(err);
  } else {
    if (!isArray(obj['buildingData'])) {
      var err = new Error('Unauthorized');
      err.status = 403;
      err.stackTraceLimit = 0;
      return next(err);
    }

    Building.count(function (err, count) {
      if (err) {
        return next(err);
      } else {
        res.json({'success': true});
        for (var i = 0; i < obj['buildingData'].length; i++) {
          var buildingObj = obj['buildingData'][i];
          buildingObj['user_id'] = obj['user_id'];
          if (count === 0) {
            createBuilding(buildingObj);
          } else {
            checkIfBuildingExistsAndUpdate(buildingObj);
          }
        };
      }
    });
  }
});

function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

function createBuilding(buildingObj) {
  Building.create(buildingObj, function (err, building) {
    if (err) {
      console.log('failed to create building!' + err);
    } else {
      console.log('created building! ' + buildingObj['building_id'] + 'level: ' + buildingObj['level']);
    }
  });
}

function checkIfBuildingExistsAndUpdate(buildingObj) {
  Building.find({'user_id': buildingObj['user_id'], 'building_id': buildingObj['building_id']}, function (err, buildingInner) {
    if (buildingInner.length == 0) {
      createBuilding(buildingObj);
    } else {
      Building.findByIdAndUpdate(ObjectId(buildingInner[0]['id']), buildingObj, {new: true}, function (err, buildingOuter) {
        if (err) {console.log(err);};
        console.log('updated building!!!!!!' + buildingOuter['id']);
      });
    }
  });
}

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  Building.findByIdAndUpdate(req.params.id, req.body, function (err, building) {
    if (err) return next(err);
    res.json(building);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  Building.findByIdAndRemove(req.params.id, req.body, function (err, building) {
    if (err) return next(err);
    res.json(building);
  });
});

module.exports = router;

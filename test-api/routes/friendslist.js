var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Friends = require('../models/Friends.js');
var User = require('../models/User.js');
var InboxItem = require('../models/InboxItem.js');
var app = require('../app');
var ObjectId = require('mongodb').ObjectId;
var Verify = require('../middleware/verify.js');

var jwt = require('jsonwebtoken');
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        var err = new Error('Unauthorized');
        err.status = 403;
        return next(err);
      } else {
        req.decoded = decoded;
        if (req.rawBody) {
          var decrypted = Verify.decrypt(req.rawBody);
          try {
            req.decryptedBody = JSON.parse(decrypted);
          } catch (e) {
            console.log(e);
            var err = new Error('Unauthorized');
            err.status = 403;
            return next(err);
          }
        }
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});

router.get('/id/:friend_request_id', function(req, res, next) {
  Friends.find(ObjectId(req.params.friend_request_id), function(err, friends){
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(friends)));
  });
});

router.get('/user_id/:user_id', function(req, res, next) {
  Friends.find({$or : [ {issuer_id: req.params.user_id}, {receiver_id: req.params.user_id} ] }).sort({created_at: -1}).limit(1000).exec(function(err, message){
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(message)));
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  var obj = req.decryptedBody;
  obj['created_at'] = Date.now();
  obj['status'] = 0;
  Friends.create(obj, function (err, friends) {
    if (err) return next(err);

    InboxItem.create({issuer_id: obj['issuer_id'], issuer_username: obj['issuer_username'],
                      receiver_id: obj['receiver_id'], receiver_username: obj['receiver_username'],
                      type: 4, taggedID: friends['_id'], created_at: Date.now()}, function (err, inboxItem) {
      if (err) return next(err);

      res.send(Verify.encrypt(JSON.stringify(friends)));
      // app.tradeRequestSent(traderequest);
      app.inboxItemSent(inboxItem);

      User.find({'_id': ObjectId(obj['receiver_id'])}, function(err, user) {
        if (err) { //console.log(err);
          return next(err);
        }

        if (user.length > 0) {
          if (user[0]['firebase_token']) {
            var notification = {};
            notification['message'] = obj['issuer_username'] +' wants to be your friend!';
            notification['device'] = user[0]['firebase_token'];

            app.sendRemoteNotification(notification);
          }
        }
      });
    });
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  Friends.findByIdAndUpdate(req.params.id, req.decryptedBody, function (err, fleet) {
    if (err) return next(err);
    res.json(fleet);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  Friends.findByIdAndRemove(req.params.id, req.decryptedBody, function (err, fleet) {
    if (err) return next(err);
    res.json(fleet);
  });
});

module.exports = router;

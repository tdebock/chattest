var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var History = require('../models/History.js');
var ObjectId = require('mongodb').ObjectId;
var User = require('../models/User.js');
var InboxItem = require('../models/InboxItem.js');
var app = require('../app');
var Verify = require('../middleware/verify.js');

var jwt = require('jsonwebtoken');
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        var err = new Error('Unauthorized');
        err.status = 403;
        return next(err);
      } else {
        req.decoded = decoded;
        if (req.rawBody) {
          var decrypted = Verify.decrypt(req.rawBody);
          try {
            req.decryptedBody = JSON.parse(decrypted);
          } catch (e) {
            console.log(e);
            var err = new Error('Unauthorized');
            err.status = 403;
            return next(err);
          }
        }
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});

/* GET /todos listing. */
// router.get('/', function(req, res, next) {
//   ChatRoom.find(function (err, chatroom) {
//     if (err) return next(err);
//     res.json(chatroom);
//   });
// });

router.get('/user_id/:user_id', function(req, res, next) {
  ChatRoom.find({members: {$elemMatch: {user_id: req.params.user_id}}}, function (err, chatroom) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(chatroom)));
  });
});

router.get('/chat_room_id/:chat_room_id', function(req, res, next) {
  ChatRoom.find(ObjectId(req.params.chat_room_id), function (err, chatroom) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(chatroom)));
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  var obj = req.decryptedBody;
  obj['created_at'] = Date.now();
  ChatRoom.create(obj, function (err, chatroom) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(chatroom)));
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  ChatRoom.findByIdAndUpdate(req.params.id, req.decryptedBody, function (err, chatroom) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(chatroom)));
  });
});

/* PUT /todos/:id */
router.put('/invitemember/:id', function(req, res, next) {
  var pendingMember = {};
  User.find({username: req.decryptedBody['username']}, function (err, user) {
    if (err) return next(err);
    pendingMember['username'] = user[0]['username'];
    pendingMember['user_id'] = (user[0]['_id']).toString();

    ChatRoom.findByIdAndUpdate(req.params.id, {$push: {pending: pendingMember}}, {new: true}, function (err, chatroom) {
      if (err) return next(err);

      // res.json(chatroom);


      InboxItem.create({issuer_id: req.decryptedBody['issuer_id'], issuer_username: req.decryptedBody['issuer_username'],
                        receiver_id: pendingMember['user_id'], receiver_username: pendingMember['username'],
                        type: 3, taggedID: chatroom['_id'], created_at: Date.now()}, function (err, inboxItem) {
        if (err) return next(err);

        res.json(chatroom);
        // app.tradeRequestSent(traderequest);
        app.inboxItemSent(inboxItem);

        // User.find({'_id': ObjectId(obj['receiver_id'])}, function(err, user) {
        //   if (err) { //console.log(err);
        //     return next(err);
        //   }
        //
        //   if (user.length > 0) {
        //     if (user[0]['ios_udid']) {
        //       var notification = {};
        //       notification['message'] = obj['issuer_username'] +' wants to trade!';
        //       notification['device'] = user[0]['ios_udid'];
        //
        //       app.sendRemoteNotification(notification);
        //     }
        //   }
        // });


      });





    });

  });
});

router.put('/acceptinvite/:id', function(req, res, next) {
  ChatRoom.findByIdAndUpdate(req.params.id, {$pull: {pending: req.decryptedBody}}, {new: true}, function (err, chatroom) {
    if (err) return next(err);
    ChatRoom.findByIdAndUpdate(req.params.id, {$push: {members: req.decryptedBody}}, {new: true}, function (err, chatroom) {
      if (err) return next(err);
      res.json(chatroom);
    });
  });
});


router.put('/declineinvite/:id', function(req, res, next) {
  ChatRoom.findByIdAndUpdate(req.params.id, {$pull: {pending: req.decryptedBody}}, {new: true}, function (err, chatroom) {
    if (err) return next(err);
    res.json(chatroom);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  ChatRoom.findByIdAndRemove(req.params.id, req.decryptedBody, function (err, chatroom) {
    if (err) return next(err);
    res.json(chatroom);
  });
});

module.exports = router;

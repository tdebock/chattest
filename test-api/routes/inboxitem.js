var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var InboxItem = require('../models/InboxItem.js');
var User = require('../models/User.js');
var ObjectId = require('mongodb').ObjectId;
var app = require('../app');
var Verify = require('../middleware/verify.js');

var jwt = require('jsonwebtoken');
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        var err = new Error('Unauthorized');
        err.status = 403;
        return next(err);
      } else {
        req.decoded = decoded;
        if (req.rawBody) {
          var decrypted = Verify.decrypt(req.rawBody);
          try {
            req.decryptedBody = JSON.parse(decrypted);
          } catch (e) {
            console.log(e);
            var err = new Error('Unauthorized');
            err.status = 403;
            return next(err);
          }
        }
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});

/* GET /todos listing. */
// router.get('/', function(req, res, next) {
//   InboxItem.find(function (err, chatroom) {
//     if (err) return next(err);
//     res.json(chatroom);
//   });
// });

router.get('/receiver_id/:receiver_id', function(req, res, next) {
  InboxItem.find({receiver_id: req.params.receiver_id}, function (err, inboxitem) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(inboxitem)));
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  InboxItem.create(req.decryptedBody, function (err, inboxitem) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(inboxitem)));
    app.inboxItemSent(inboxitem);

    User.find({'_id': ObjectId(inboxitem['receiver_id'])}, function(err, user) {
      if (err) { //console.log(err);
        return next(err);
      }

      if (user.length > 0) {
        if (user[0]['ios_udid']) {
          var notification = {};
          if (inboxitem['type'] == 1) {
            notification['message'] = inboxitem['issuer_username'] +' sent you a private message.';
          } else if (inboxitem['type'] == 2) {

          }

          notification['device'] = user[0]['ios_udid'];

          app.sendRemoteNotification(notification);
        }
      }
    });

  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  InboxItem.findByIdAndUpdate(req.params.id, req.decryptedBody, function (err, inboxitem) {
    if (err) return next(err);
    res.json(inboxitem);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  InboxItem.findByIdAndRemove(req.params.id, req.decryptedBody, function (err, inboxitem) {
    if (err) return next(err);
    res.json(inboxitem);
  });
});

module.exports = router;

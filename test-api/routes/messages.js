var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Message = require('../models/Message.js');
var Verify = require('../middleware/verify.js');
/* GET /todos listing. */

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        var err = new Error('Unauthorized');
        err.status = 403;
        return next(err);
      } else {
        req.decoded = decoded;
        if (req.rawBody) {
          var decrypted = Verify.decrypt(req.rawBody);
          try {
            req.decryptedBody = JSON.parse(decrypted);
          } catch (e) {
            console.log(e);
            var err = new Error('Unauthorized');
            err.status = 403;
            return next(err);
          }
        }
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});

// router.get('/', function(req, res, next) {
//   Message.find(function (err, message) {
//     if (err) return next(err);
//     res.json(message);
//   });
// });

router.get('/fleet_id/:fleet_id', function(req, res, next) {
  Message.find({fleet_id: req.params.fleet_id}).sort({created_at: -1}).limit(50).exec(function(err, message){
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(message)));
  });
});

router.get('/chat_room_id/:chat_room_id', function(req, res, next) {
  Message.find({chat_room_id: req.params.chat_room_id}).sort({created_at: -1}).limit(50).exec(function(err, message){
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(message)));
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  Message.create(req.decryptedBody, function (err, message) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(message)));
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  Message.findByIdAndUpdate(req.params.id, req.decryptedBody, function (err, message) {
    if (err) return next(err);
    res.json(message);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  Message.findByIdAndRemove(req.params.id, req.decryptedBody, function (err, message) {
    if (err) return next(err);
    res.json(message);
  });
});

module.exports = router;

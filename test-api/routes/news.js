var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var News = require('../models/News.js');
/* GET /todos listing. */

var jwt = require('jsonwebtoken');
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});


router.get('/', function(req, res, next) {
  News.find(function (err, news) {
    if (err) return next(err);
    res.json(news);
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  News.create(req.body, function (err, news) {
    if (err) return next(err);
    res.json(news);
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  News.findByIdAndUpdate(req.params.id, req.body, function (err, news) {
    if (err) return next(err);
    res.json(news);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  News.findByIdAndRemove(req.params.id, req.body, function (err, news) {
    if (err) return next(err);
    res.json(news);
  });
});

module.exports = router;

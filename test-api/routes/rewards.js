var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Reward = require('../models/Reward.js');
/* GET /todos listing. */

var jwt = require('jsonwebtoken');
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});


router.get('/', function(req, res, next) {
  Reward.find(function (err, reward) {
    if (err) return next(err);
    res.json(reward);
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  Reward.create(req.body, function (err, reward) {
    if (err) return next(err);
    res.json(reward);
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  Reward.findByIdAndUpdate(req.params.id, req.body, function (err, reward) {
    if (err) return next(err);
    res.json(reward);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  Reward.findByIdAndRemove(req.params.id, req.body, function (err, reward) {
    if (err) return next(err);
    res.json(reward);
  });
});

module.exports = router;

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var TradeRequest = require('../models/TradeRequest.js');
var User = require('../models/User.js');
var InboxItem = require('../models/InboxItem.js');
var app = require('../app');
var ObjectId = require('mongodb').ObjectId;
var Verify = require('../middleware/verify.js');

var jwt = require('jsonwebtoken');
var config = require('../config');

const TRADE_REQUEST_AWAITING_REQUEST = 1;
const TRADE_REQUEST_OPEN = 2;
const TRADE_REQUEST_WAITING_FIRST_CONRIFMATION = 3;
const TRADE_REQUEST_WAITING_FINAL_CONFIRMATION = 4;
const TRADE_REQUEST_COMPLETE = 5;
const TRADE_REQUEST_CANCELLED = 6;
const TRADE_REQUEST_EXPIRED = 7;

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        var err = new Error('Unauthorized');
        err.status = 403;
        return next(err);
      } else {
        req.decoded = decoded;
        if (req.rawBody) {
          var decrypted = Verify.decrypt(req.rawBody);
          try {
            req.decryptedBody = JSON.parse(decrypted);
          } catch (e) {
            console.log(e);
            var err = new Error('Unauthorized');
            err.status = 403;
            return next(err);
          }
        }
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});

/* GET /todos listing. */
// router.get('/', function(req, res, next) {
//   TradeRequest.find(function (err, traderequest) {
//     if (err) return next(err);
//     res.json(traderequest);
//   });
// });

router.get('/trade_request_id/:trade_request_id', function(req, res, next) {
  TradeRequest.find(ObjectId(req.params.trade_request_id), function (err, traderequest) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(traderequest)));
  });
});

router.get('/user_id/:user_id', function(req, res, next) {
  TradeRequest.find({$or : [ {issuer_id: req.params.user_id}, {receiver_id: req.params.user_id} ] }).sort({created_at: -1}).limit(300).exec(function(err, traderequest){
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(traderequest)));
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  var obj = req.decryptedBody;
  var fields = ['issuer_id', 'issuer_username', 'receiver_id', 'receiver_username'];
  if (!Verify.validate(obj, fields)) {
    var err = new Error('Unauthorized');
    err.status = 403;
    err.stackTraceLimit = 0;
    return next(err);
  } else {
    obj['created_at'] = Date.now();
    obj['status'] = TRADE_REQUEST_AWAITING_REQUEST;
    TradeRequest.create(obj, function (err, traderequest) {
      if (err) return next(err);

      InboxItem.create({issuer_id: obj['issuer_id'], issuer_username: obj['issuer_username'],
                        receiver_id: obj['receiver_id'], receiver_username: obj['receiver_username'],
                        type: 2, taggedID: traderequest['_id'], created_at: Date.now()}, function (err, inboxItem) {
        if (err) return next(err);

        res.send(Verify.encrypt(JSON.stringify(traderequest)));
        app.tradeRequestSent(traderequest);
        app.inboxItemSent(inboxItem);

        User.find({'_id': ObjectId(obj['receiver_id'])}, function(err, user) {
          if (err) { //console.log(err);
            return next(err);
          }

          if (user.length > 0) {
            if (user[0]['firebase_token']) {
              var notification = {};
              notification['message'] = obj['issuer_username'] +' wants to trade!';
              notification['device'] = user[0]['firebase_token'];

              app.sendRemoteNotification(notification);
            }
          }
        });
      });
    });
  }
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  var tradeReqID = req.params.id;
  var tradeReqBody = req.decryptedBody;

  TradeRequest.findByIdAndUpdate(tradeReqID, tradeReqBody, {new: true}, function (err, traderequest) {
    if (err) return next(err);

    if (traderequest['issuer_removed'] == 1 && traderequest['receiver_removed'] == 1) {
      TradeRequest.findByIdAndRemove(tradeReqID, function (err, traderequest) {
        if (err) return next(err);
        console.log('trade request deleted');
        return res.send(Verify.encrypt(JSON.stringify(traderequest)));
      });
    } else {

      if (traderequest['status'] != TRADE_REQUEST_EXPIRED) {
        if ((traderequest['issuer_removed'] == 1 || traderequest['receiver_removed'] == 1) && traderequest['status'] <= TRADE_REQUEST_WAITING_FINAL_CONFIRMATION) {
          tradeReqBody['status'] = TRADE_REQUEST_CANCELLED;
        } else if (traderequest['issuer_accept'] == 0 && traderequest['receiver_accept'] == 0) {
          if (traderequest['issuer_loot'].length == 0 && traderequest['receiver_loot'].length == 0) {
            tradeReqBody['status'] = TRADE_REQUEST_OPEN;
          } else {
            tradeReqBody['status'] = TRADE_REQUEST_WAITING_FIRST_CONRIFMATION;
          }
        } else if (traderequest['issuer_accept'] == 1 && traderequest['receiver_accept'] == 1) {
          tradeReqBody['status'] = TRADE_REQUEST_COMPLETE;
        } else if (traderequest['issuer_accept'] == 1 || traderequest['receiver_accept'] == 1) {
          tradeReqBody['status'] = TRADE_REQUEST_WAITING_FINAL_CONFIRMATION;
        }
      }

      TradeRequest.findByIdAndUpdate(tradeReqID, tradeReqBody, function (err2, traderequest2) {
        if (err2) return next(err2);
        res.send(Verify.encrypt(JSON.stringify(traderequest2)));
        app.tradeRequestUpdated(traderequest2, {'trade_request_id': tradeReqID, 'modifications': tradeReqBody});
      });
    }
  });
});

/* DELETE /todos/:id */
// router.delete('/:id', function(req, res, next) {
//   TradeRequest.findByIdAndRemove(req.params.id, req.body, function (err, traderequest) {
//     if (err) return next(err);
//     res.json(traderequest);
//   });
// });

module.exports = router;

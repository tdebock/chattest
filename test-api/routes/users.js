var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/User.js');
var Verify = require('../middleware/verify.js');
var ObjectId = require('mongodb').ObjectId;

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../config');

router.use(function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token) {
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        var err = new Error('Unauthorized');
        err.status = 403;
        return next(err);
      } else {
        req.decoded = decoded;
        if (req.rawBody) {
          var decrypted = Verify.decrypt(req.rawBody);
          try {
            req.decryptedBody = JSON.parse(decrypted);
          } catch (e) {
            console.log(e);
            var err = new Error('Unauthorized');
            err.status = 403;
            return next(err);
          }
        }
        next();
      }
    });
  } else {
    var err = new Error('Unauthorized');
    err.status = 403;
    return next(err);
  }
});

/* GET /todos listing. */
router.post('/random', function(req, res, next) {
  var jsonBody = req.decryptedBody;

  User.find({"_id": {$ne: ObjectId(jsonBody['user_id'])}}).count().exec(function (err, count) {
    if (err) return next(err);
    console.log('count of users not equal: ' + count);
    if (count === 0) {
      res.send(Verify.encrypt(JSON.stringify({'response': 'NO USERS!!!'})));
    } else {
      var random = Math.floor(Math.random() * count)
      console.log('random entry: ' + random);

      User.findOne({"_id": {$ne: ObjectId(jsonBody['user_id'])}}).skip(random).exec(
        function (err, user) {
          if (err) return next(err);
          res.send(Verify.encrypt(JSON.stringify(user)));
      })
    }
  })
});

router.post('/username', function(req, res, next) {
  var jsonBody = req.decryptedBody;
  var fields = ['username'];
  if (!Verify.validate(jsonBody, fields)) {
    var err = new Error('Unauthorized');
    err.status = 403;
    err.stackTraceLimit = 0;
    return next(err);
  } else {
    console.log('fetching user: ' + jsonBody['username']);
    User.find({username: jsonBody['username']}, function (err, user) {
      if (err) return next(err);
      res.send(Verify.encrypt(JSON.stringify(user)));
    });
  }
});

/* POST /todos */
router.post('/', function(req, res, next) {
  var jsonBody = req.decryptedBody;
  var fields = ['username'];
  if (!Verify.validate(jsonBody, fields)) {
    var err = new Error('Unauthorized');
    err.status = 403;
    err.stackTraceLimit = 0;
    return next(err);
  } else {
    console.log('adding user: ' + jsonBody['username']);
    User.create(jsonBody, function (err, user) {
      if (err) return next(err);
      res.send(Verify.encrypt(JSON.stringify(user)));
    });
  }
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  var jsonBody = req.decryptedBody;
  User.findByIdAndUpdate(req.params.id, jsonBody, {new: true}, function (err, user) {
    if (err) return next(err);
    res.send(Verify.encrypt(JSON.stringify(user)));
  });
});

/* DELETE /todos/:id */
// router.delete('/:id', function(req, res, next) {
//   User.findByIdAndRemove(req.params.id, req.body, function (err, user) {
//     if (err) return next(err);
//     res.json(user);
//   });
// });

module.exports = router;

var mongoose = require('mongoose')
var app = require('express')();
// var app = require('./app').app;

var fs = require('fs');
var options = {
  key: fs.readFileSync('./OTA/key.pem'),
  cert: fs.readFileSync('./OTA/server.crt')
};
var Verify = require('./middleware/verify.js');

// var https = require('https').Server(options, app);
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Message = require('./models/Message.js');

io.on('connection', function(socket){
  // console.log('a user connected');
  socket.on('disconnect', function(){
		// console.log('a user disconnected');
  });

	socket.on('join_room', function (room) {
	        socket.join(room);
					// console.log('joined room: ' + room);
	});

	socket.on('leave_room', function (room) {
	        socket.leave(room);
					// console.log('left room: ' + room);
	});

  socket.on('chat message', function(msg){
		gotMessage(msg);
  });
});

function gotMessage(msg) {
  try {
    var obj = JSON.parse(Verify.decrypt(msg));
    var messageString = obj['message'];
  	var userIDString = obj['user_id'];
  	var usernameString = obj['username'];
  	var fleetIDString = obj['fleet_id'];
  	var chat_room_idString = obj['chat_room_id'];
  	var dateString = Date.now();

  	obj['created_at'] = Date.now();

  	// io.sockets.in('browser_listener').emit('test listener', obj);

  	if (chat_room_idString != 'global_chat') {

      Message.create({
  			user_id: userIDString,
  			username: usernameString,
  			message: messageString,
  			fleet_id: fleetIDString,
  			chat_room_id: chat_room_idString,
  			created_at: Date.now() }, function(err,message){
          if (err) {
            console.log('failed to create message');
          } else {
    				if (chat_room_idString == '') {
    					io.sockets.in('fleet_id=' + fleetIDString).emit('new message', Verify.encrypt(JSON.stringify(message)));
    				} else {
    					io.sockets.in('chat_room_id=' + chat_room_idString).emit('new message', Verify.encrypt(JSON.stringify(message)));
    				}

          }
        });

  	} else {
  		io.sockets.in('chat_room_id=' + chat_room_idString).emit('new message', Verify.encrypt(JSON.stringify(obj)));
  	}
  } catch (e) {
    console.log(e);
  }
}

app.get('/', function(req, res){
  res.sendfile('socketListener.html');
});

http.listen(8080, function(){
  console.log('listening on *:8080');
});

exports.receiveTradeRequest = function(tradeRequest) {
  var issuer_id = tradeRequest['issuer_id'];
  var encrypted = Verify.encrypt(JSON.stringify(tradeRequest));
  // var receiver_id = tradeRequest['receiver_id'];

  io.sockets.in('user_id=' + issuer_id).emit('receive trade request', encrypted);
  // io.sockets.in('user_id=' + receiver_id).emit('receive trade request', tradeRequest);
};

exports.updateTradeRequest = function(tradeRequest, modifications) {
  var issuer_id = tradeRequest['issuer_id'];
  var receiver_id = tradeRequest['receiver_id'];
  var encrypted = Verify.encrypt(JSON.stringify(modifications));

  io.sockets.in('user_id=' + issuer_id).emit('update trade request', encrypted);
  io.sockets.in('user_id=' + receiver_id).emit('update trade request', encrypted);
};

exports.receiveInboxItem = function(inboxItem) {
  var receiver_id = inboxItem['receiver_id'];
  var encrypted = Verify.encrypt(JSON.stringify(inboxItem));

  io.sockets.in('user_id=' + receiver_id).emit('receive inbox item', encrypted);
};
